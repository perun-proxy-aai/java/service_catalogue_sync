package cz.muni.ics.serviceslistsync.service;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class Result {
    private int created = 0;
    private int updated = 0;
    private int deleted = 0;
    private int errors = 0;

    public void incCreated() {
        this.created++;
    }

    public void incUpdated() {
        this.updated++;
    }

    public void incDeleted() {
        this.deleted++;
    }

    public void incDeleted(int amount) {
        this.deleted += amount;
    }

    public void incErrors() {
        this.errors++;
    }

    public boolean needsGenerateFeed() {
        return created > 0 || updated > 0 || deleted > 0;
    }

    @Override
    public String toString() {
        return "Results - " +
            "created: " + created +
            ", updated: " + updated +
            ", deleted: " + deleted +
            ", errors: " + errors;
    }

}
