package cz.muni.ics.serviceslistsync.service.sub;

import cz.muni.ics.serviceslistsync.perun.AttrsMapping;
import cz.muni.ics.serviceslistsync.service.ProcessingProperties;
import cz.muni.ics.serviceslistsync.common.exceptions.RpIdentifierIllegalStateException;
import cz.muni.ics.serviceslistsync.data.RelyingServicesRepository;
import cz.muni.ics.serviceslistsync.data.enums.RelyingServiceEnvironment;
import cz.muni.ics.serviceslistsync.data.enums.RelyingServiceProtocol;
import cz.muni.ics.serviceslistsync.data.model.RelyingServiceDTO;
import cz.muni.ics.serviceslistsync.perun.exception.PerunConnectionException;
import cz.muni.ics.serviceslistsync.perun.exception.PerunUnknownException;
import cz.muni.ics.serviceslistsync.perun.model.FacilityWithAttributes;
import cz.muni.ics.serviceslistsync.perun.model.PerunAttributeValue;
import cz.muni.ics.serviceslistsync.perun.rpc.PerunAdapter;
import cz.muni.ics.serviceslistsync.service.Result;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ServiceSynchronizer {

    public static final String EN = "en";

    // FIELDS

    private final PerunAdapter perunAdapter;

    private final AttrsMapping perunAttrNames;

    private final RelyingServicesRepository servicesRepository;

    private final ProcessingProperties processingProperties;

    @Autowired
    public ServiceSynchronizer(@NonNull PerunAdapter perunAdapter,
                               @NonNull ProcessingProperties processingProperties,
                               @NonNull AttrsMapping perunAttrNames,
                               @NonNull RelyingServicesRepository servicesRepository)
    {
        this.perunAdapter = perunAdapter;
        this.perunAttrNames = perunAttrNames;
        this.servicesRepository = servicesRepository;
        this.processingProperties = processingProperties;
    }

    public Result process() {
        log.info("Started synchronization to DB");
        Result res = new Result();
        for (String proxyIdentifierValue: processingProperties.getProxyIdentifierValues()) {
            Set<FacilityWithAttributes> facilities;
            try {
                facilities = new HashSet<>(perunAdapter.getFacilitiesByAttributeWithAttributes(
                        perunAttrNames.getProxyIdentifier(), proxyIdentifierValue, perunAttrNames.getNames()));
            } catch (PerunConnectionException | PerunUnknownException e) {
                log.error("Caught exception when fetching facilities by attr '{}' with value '{}' with attributes '{}'",
                        perunAttrNames.getProxyIdentifier(), proxyIdentifierValue, perunAttrNames.getNames(), e);
                return res;
            }
            log.info("Processing facilities");
            Set<String> foundIdentifiers = new HashSet<>();
            for (FacilityWithAttributes f : facilities) {
                processService(f, foundIdentifiers, res);
            }
            deleteServices(foundIdentifiers, res);
        }
        return res;
    }

    private void deleteServices(Set<String> foundIdentifiers, Result res) {
        if (!processingProperties.isDelete()) {
            log.debug("Deleting is disabled");
            return;
        }
        Set<String> identifiersToDelete = getRpIdentifiersToDelete(foundIdentifiers);
        servicesRepository.deleteAllByRpIdentifierIn(identifiersToDelete);
        log.debug("Deleted entries with identifiers {}", identifiersToDelete);
        res.incDeleted(identifiersToDelete.size());
    }

    private Set<String> getRpIdentifiersToDelete(Collection<String> usedIds) {
        Set<String> identifiers = servicesRepository.findAll()
                .stream()
                .map(RelyingServiceDTO::getRpIdentifier)
                .collect(Collectors.toSet());
        identifiers.removeAll(usedIds);
        return identifiers;
    }

    private void processService(FacilityWithAttributes f, Set<String> usedIds, Result res) {
        try {
            if (f == null) {
                log.warn("NULL facility given, generating error and continue processing");
                res.incErrors();
                return;
            }
            log.debug("Processing facility '{}'", f);
            boolean isOidc = getBooleanValue(perunAttrNames.getIsOidc(), f.getAttributes());
            boolean isSaml = getBooleanValue(perunAttrNames.getIsSaml(), f.getAttributes());
            if (isOidc) {
                String oidcClientId = processOidcService(f, res);
                if (StringUtils.hasText(oidcClientId)) {
                    usedIds.add(oidcClientId);
                }
            } else if (isSaml) {
                String oidcClientId = processSamlService(f, res);
                if (StringUtils.hasText(oidcClientId)) {
                    usedIds.add(oidcClientId);
                }
            } else {
                log.debug("Facility is not SAML nor OIDC service, skip it.");
            }
        } catch (Exception e) {
            log.warn("Caught exception when syncing facility {}", f, e);
            res.incErrors();
        }
    }
    
    private String processOidcService(FacilityWithAttributes f, Result res) throws RpIdentifierIllegalStateException {
        String clientId = getStringValue(perunAttrNames.getOidcClientId(), f.getAttributes());
        if (!StringUtils.hasText(clientId)) {
            throw new RpIdentifierIllegalStateException("Facility " + f.getFacility().getId() +
                    " is marked as OIDC facility, but has not client_id set");
        }
        return processService(f, clientId, RelyingServiceProtocol.OIDC, res);
    }

    private String processSamlService(FacilityWithAttributes f, Result res) throws RpIdentifierIllegalStateException {
        String entityId = getStringValue(perunAttrNames.getSamlEntityId(), f.getAttributes());
        if (!StringUtils.hasText(entityId)) {
            throw new RpIdentifierIllegalStateException("Facility " + f.getFacility().getId() +
                    " is marked as SAML facility, but has not entityID set");
        }
        return processService(f, entityId, RelyingServiceProtocol.SAML, res);
    }

    private String processService(FacilityWithAttributes f, String rpIdentifier, RelyingServiceProtocol protocol, Result res) {
        RelyingServiceDTO dto = servicesRepository.findByRpIdentifier(rpIdentifier)
                .orElse(null);
        boolean isNew = dto == null;
        if (isNew) {
            if (!processingProperties.isCreate()) {
                log.debug("Create is disabled, skipping RP with identifier {}", rpIdentifier);
                return null;
            }
            dto = new RelyingServiceDTO();
        } else if (!processingProperties.isUpdate()) {
            log.debug("Updating is disabled, skipping RP with identifier {}", rpIdentifier);
            return null;
        }

        processGeneralService(dto, f.getAttributes());
        dto.setRpIdentifier(rpIdentifier);
        dto.setProtocol(protocol);
        servicesRepository.save(dto);
        if (isNew) {
            log.debug("Created new entry for RP with identifier {}", rpIdentifier);
            res.incCreated();
        } else {
            log.debug("Updated entry for RP with identifier {}", rpIdentifier);
            res.incUpdated();
        }
        return rpIdentifier;
    }

    private void processGeneralService(RelyingServiceDTO dto, Map<String, PerunAttributeValue> attributes) {
        setServiceInformation(dto, attributes);
        setServicePolicies(dto, attributes);
        setServiceLinks(dto, attributes);
        setOrganizationInfo(dto, attributes);
        setContacts(dto, attributes);
    }

    // SET FIELDS
    
    private void setServiceInformation(RelyingServiceDTO s, Map<String, PerunAttributeValue> attrs) {
        s.setName(getMapValue(perunAttrNames.getName(), attrs));
        s.setDescription(getMapValue(perunAttrNames.getDescription(), attrs));
        boolean isTestSp = getBooleanValue(perunAttrNames.getIsTestSp(), attrs);
        s.setEnvironment(isTestSp ? RelyingServiceEnvironment.TEST : RelyingServiceEnvironment.PRODUCTION);
    }

    private void setServicePolicies(RelyingServiceDTO s, Map<String, PerunAttributeValue> attrs) {
        String privacyPolicy = getStringValue(perunAttrNames.getPrivacyPolicy(), attrs);
        if (StringUtils.hasText(privacyPolicy)) {
            s.setPrivacyPolicy(Map.of(EN, privacyPolicy));
        }
        String termsPolicy = getStringValue(perunAttrNames.getTermsPolicy(), attrs);
        if (StringUtils.hasText(termsPolicy)) {
            s.setPrivacyPolicy(Map.of(EN, termsPolicy));
        }
        String irpPolicy = getStringValue(perunAttrNames.getIrpPolicy(), attrs);
        if (StringUtils.hasText(irpPolicy)) {
            s.setPrivacyPolicy(Map.of(EN, irpPolicy));
        }
        s.setJurisdiction(getStringValue(perunAttrNames.getJurisdiction(), attrs));
    }

    private void setServiceLinks(RelyingServiceDTO s, Map<String, PerunAttributeValue> attrs) {
        s.setLoginUrl(getStringValue(perunAttrNames.getLoginLink(), attrs));
        s.setWebsiteUrl(getStringValue(perunAttrNames.getServiceWebsite(), attrs));
    }

    private void setOrganizationInfo(RelyingServiceDTO s, Map<String, PerunAttributeValue> attrs) {
        s.setProvidingOrganization(getMapValue(perunAttrNames.getOrgName(), attrs));
        String orgWebsite = getStringValue(perunAttrNames.getOrgUrl(), attrs);
        if (StringUtils.hasText(orgWebsite)) {
            s.setProvidingOrganizationWebsite(Map.of(EN, orgWebsite));
        }
    }

    private void setContacts(RelyingServiceDTO s, Map<String, PerunAttributeValue> attrs) {
        s.setAdministrativeContact(getStringValue(perunAttrNames.getAdminContact(), attrs));
        List<String> techContacts = getListValue(perunAttrNames.getTechContact(), attrs);
        if (techContacts != null && !techContacts.isEmpty()) {
            s.setTechnicalContact(techContacts.get(0));
        }
        List<String> securityContacts = getListValue(perunAttrNames.getSecurityContact(), attrs);
        if (securityContacts != null && !securityContacts.isEmpty()) {
            s.setSecurityContact(securityContacts.get(0));
        }
        List<String> helpdeskContacts = getListValue(perunAttrNames.getSupportContact(), attrs);
        if (helpdeskContacts != null && !helpdeskContacts.isEmpty()) {
            s.setHelpdeskContact(helpdeskContacts.get(0));
        }
    }

    // HELPER METHODS

    private String getStringValue(String attr, Map<String, PerunAttributeValue> attrs) {
        PerunAttributeValue emptyValue = PerunAttributeValue.emptyStringValue(attr);
        return attrs.getOrDefault(attr, emptyValue).valueAsString();
    }

    private List<String> getListValue(String attr, Map<String, PerunAttributeValue> attrs) {
        PerunAttributeValue emptyValue = PerunAttributeValue.emptyListValue(attr);
        return attrs.getOrDefault(attr, emptyValue).valueAsList();
    }

    private boolean getBooleanValue(String attr, Map<String, PerunAttributeValue> attrs) {
        PerunAttributeValue emptyValue = PerunAttributeValue.emptyBooleanValue(attr);
        return attrs.getOrDefault(attr, emptyValue).valueAsBoolean();
    }

    private Map<String, String> getMapValue(String attr, Map<String, PerunAttributeValue> attrs) {
        PerunAttributeValue emptyValue = PerunAttributeValue.emptyMapValue(attr);
        return attrs.getOrDefault(attr, emptyValue).valueAsMap();
    }

}
