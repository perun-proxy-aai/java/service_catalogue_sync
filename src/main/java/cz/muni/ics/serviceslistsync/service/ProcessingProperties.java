package cz.muni.ics.serviceslistsync.service;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@ConstructorBinding
@ConfigurationProperties(prefix = "processing")
@Configuration
@Slf4j
@Validated
public class ProcessingProperties {

    @NotNull
    @NotEmpty
    private Set<String> proxyIdentifierValues;

    boolean create = true;

    boolean update = true;

    boolean delete = true;

    @PostConstruct
    public void init() {
        log.info("Initialized ProcessingProperties");
        log.debug("{}", this);
    }

}
