package cz.muni.ics.serviceslistsync.perun;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotBlank;
import java.util.Collection;
import java.util.Set;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@ConfigurationProperties("attributes")
@Configuration
@Validated
public class AttrsMapping {

    // Service basic info

    @NotBlank private String name;

    @NotBlank private String description;

    @NotBlank private String isTestSp;

    // Links

    @NotBlank private String loginLink;

    @NotBlank private String serviceWebsite;

    @NotBlank private String jurisdiction;

    // Policies

    @NotBlank private String privacyPolicy;

    @NotBlank private String termsPolicy;

    @NotBlank private String irpPolicy;

    // Organization

    @NotBlank private String orgName;

    @NotBlank private String orgUrl;

    // Contacts

    @NotBlank private String adminContact;

    @NotBlank private String techContact;

    @NotBlank private String supportContact;

    @NotBlank private String securityContact;

    // RP Identifier
    @NotBlank private String oidcClientId;

    // SAML
    @NotBlank private String samlEntityId;

    // app related

    @NotBlank private String proxyIdentifier;

    @NotBlank private String isSaml;

    @NotBlank private String isOidc;

    public Collection<String> getNames() {
        return Set.of(
            name,
            description,
            isTestSp,
            loginLink,
            serviceWebsite,
            jurisdiction,
            privacyPolicy,
            termsPolicy,
            irpPolicy,
            orgName,
            orgUrl,
            adminContact,
            techContact,
            supportContact,
            securityContact,
            oidcClientId,
            samlEntityId,
            proxyIdentifier,
            isSaml,
            isOidc
        );
    }

    @PostConstruct
    public void postInit() {
        log.info("Initialized Attribute names properties");
        log.debug("{}", this);
    }

}
