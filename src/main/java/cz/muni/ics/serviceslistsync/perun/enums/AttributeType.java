package cz.muni.ics.serviceslistsync.perun.enums;

/**
 * Represents type of Value in Attribute from Perun.
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
public enum AttributeType {
    STRING,
    INTEGER,
    BOOLEAN,
    ARRAY,
    MAP
}
