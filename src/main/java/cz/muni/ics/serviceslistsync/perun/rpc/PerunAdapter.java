package cz.muni.ics.serviceslistsync.perun.rpc;

import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.serviceslistsync.perun.exception.PerunConnectionException;
import cz.muni.ics.serviceslistsync.perun.exception.PerunUnknownException;
import cz.muni.ics.serviceslistsync.perun.model.FacilityWithAttributes;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the Perun RPC adapter.
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
@Component
@Slf4j
public class PerunAdapter {

    // MANAGERS
    public static final String FACILITIES_MANAGER = "facilitiesManager";

    // METHODS

    public static final String GET_FACILITIES_BY_ATTRIBUTE_WITH_ATTRIBUTES = "getFacilitiesByAttributeWithAttributes";

    // PARAMS

    public static final String PARAM_ATTR_NAMES = "attrNames";

    public static final String PARAM_ATTRIBUTE_NAME = "attributeName";

    private static final String PARAM_ATTRIBUTE_VALUE = "attributeValue";

    // fields

    private final PerunConnector perunConnector;

    @Autowired
    public PerunAdapter(@NonNull PerunConnector perunConnector) {
        this.perunConnector = perunConnector;
    }


    /**
     * Get facility with specified attribute set to specified value and also fetch listed attributes.
     * @param attrName URN of the filter attribute
     * @param attrValue value for the filter attribute
     * @param attributes list of URNs for attributes to fetch
     * @return All matching facilities with specified attributes
     * @throws PerunUnknownException when some unknown exception occurs in connector
     * @throws PerunConnectionException when connection exception occurs in connector
     */
    public List<FacilityWithAttributes> getFacilitiesByAttributeWithAttributes(@NonNull String attrName,
                                                                               @NonNull String attrValue,
                                                                               Collection<String> attributes)
            throws PerunUnknownException, PerunConnectionException
    {
        Map<String, Object> params = new LinkedHashMap<>();
        params.put(PARAM_ATTRIBUTE_NAME, attrName);
        params.put(PARAM_ATTRIBUTE_VALUE, attrValue);
        params.put(PARAM_ATTR_NAMES, attributes);

        JsonNode perunResponse = perunConnector.post(
                FACILITIES_MANAGER, GET_FACILITIES_BY_ATTRIBUTE_WITH_ATTRIBUTES, params
        );

        return PerunMapper.mapFacilitiesWithAttributes(perunResponse);
    }

}
