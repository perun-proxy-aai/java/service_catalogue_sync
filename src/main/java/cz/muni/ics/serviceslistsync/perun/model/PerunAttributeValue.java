package cz.muni.ics.serviceslistsync.perun.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import cz.muni.ics.serviceslistsync.perun.enums.AttributeType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

/**
 * Model representing value of attribute from Perun.
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class PerunAttributeValue extends PerunAttributeValueAwareModel {

    public static final String FIELD_ATTR_NAME = "attrName";

    @NonNull private String attrName;

    public PerunAttributeValue(String attrName, String type, JsonNode value) {
        super(type, value);
        this.setAttrName(attrName);
    }

    public PerunAttributeValue(String attrName, AttributeType type, JsonNode value) {
        super(type, value);
        this.setAttrName(attrName);
    }

    public static PerunAttributeValue emptyStringValue(String attrName) {
        return new PerunAttributeValue(
            attrName, AttributeType.STRING, JsonNodeFactory.instance.nullNode());
    }

    public static PerunAttributeValue emptyIntegerValue(String attrName) {
        return new PerunAttributeValue(
            attrName, AttributeType.INTEGER, JsonNodeFactory.instance.nullNode());
    }

    public static PerunAttributeValue emptyBooleanValue(String attrName) {
        return new PerunAttributeValue(
            attrName, AttributeType.BOOLEAN, JsonNodeFactory.instance.nullNode());
    }

    public static PerunAttributeValue emptyListValue(String attrName) {
        return new PerunAttributeValue(
            attrName, AttributeType.ARRAY, JsonNodeFactory.instance.nullNode());
    }

    public static PerunAttributeValue emptyMapValue(String attrName) {
        return new PerunAttributeValue(
            attrName, AttributeType.MAP, JsonNodeFactory.instance.nullNode());
    }

}
