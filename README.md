## Service catalogue sync script

JAR "script" that fetches information about services (Relying parties) and updates the registered data in the Service Catalogue MongoDB database

Related project - [Relying Services catalogue](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue)

### Configuration

See config-templates directory for more information.

### Running

- **CONFIG_FILE_NAME**: Name of the configuration file (e.g. `application-test.yml`) without any extension
  - **example**: `application-test`
- **CONFIG_DIR_LOCATION**: Filesystem path where the configuration file is located. Must end with a slash.
  - **example**: `/etc/sync-app/`

`java -jar service-catalogue-sync.jar --spring.config.name=<CONFIG_FILE_NAME> --spring.confgi.location=<CONFIG_DIR_LOCATION>`
